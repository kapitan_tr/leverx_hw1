import { Fragment } from "react"

const InputButton = (props) => {

  const getParams = () => {
    if (typeof props.value === "number") {
      return props.action(props.value);
    } else {
      return props.action(props.value, props.title);
    }
  }

  return (
    <Fragment>
      <button className={`w-100 py-3 btn btn-primary`} onClick={() => getParams()}>
        {props.title}
      </button>
    </Fragment>
  )
}

export default InputButton;