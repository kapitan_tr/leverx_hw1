##############################
#docker file for lab lesson 2#
##############################
# set the base imege to noda
FROM node:latest AS doc1
LABEL maintainer="Trosko Eugene"
WORKDIR /calc
COPY . .
RUN npm install
RUN npm run build

FROM node:latest
LABEL maintainer="Trosko Eugene"
WORKDIR /calc
COPY --from=doc1 /calc/build build
COPY --from=doc1 /calc/node_modules node_modules
RUN npm install -g serve
CMD serve -s build
